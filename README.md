# lara-pdf-merger

Original written by http://pdfmerger.codeplex.com/team/view

This Package was tested on **Laravel 5.8.13**

Working with **PHP 7.2**

### Improvements 

* Code source refactoring
* Enabling the Facade use
* Adding duplex merge feature
* Seperate save operation from the merge
  
## Installation

* Require this package in your composer.json by adding those lines

```
{
    "require": {
        "awesomeproject/pdfmerge": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/jenya_nonono/pdfmerge"
        }
    ]
}
```

After updating composer, add the ServiceProvider to the providers array in config/app.php
```php
    AwesomeProject\PdfMerger\PdfMergerServiceProvider::class,
```
You can optionally use the facade for shorter code. Add this to your facades:
```php
    'PdfMerger' => AwesomeProject\PdfMerger\Facades\PdfMerger::class,
```
## Usage

```php

use AwesomeProject\PdfMerger\Facades\PdfMerger;

$pdfMerger = PDFMerger::init(); //Initialize the merger

$pdfMerger->addPDF('samplepdfs/one.pdf', '1, 3, 4');
$pdfMerger->addPDF('samplepdfs/two.pdf', '1-2');
$pdfMerger->addPDF('samplepdfs/three.pdf', 'all');

//You can optionally specify a different orientation for each PDF
$pdfMerger->addPDF('samplepdfs/one.pdf', '1, 3, 4', 'L');
$pdfMerger->addPDF('samplepdfs/two.pdf', '1-2', 'P');

$pdfMerger->merge(); //For a normal merge (No blank page added)

// OR..
$pdfMerger->duplexMerge(); //Merges your provided PDFs and adds blank pages between documents as needed to allow duplex printing

// optional parameter can be passed to the merge functions for orientation (P for protrait, L for Landscape). 
// This will be used for every PDF that doesn't have an orientation specified

$pdfMerger->save("file_path.pdf");

// OR...
$pdfMerger->save("file_name.pdf", "download");
// REPLACE 'download' WITH 'browser', 'download', 'string', or 'file' for output options

```

## Authors
* [RamonSmit](https://github.com/RamonSmit)
* [MarwenSami](https://github.com/MarwenSami)
* [Jenyaukraine](https://github.com/jenyaukraine)

## Credits
* **deltaaskii** [deltaaskii/lara-pdf-merger](https://github.com/deltaaskii/lara-pdf-merger)
* **DALTCORE** [DALTCORE/lara-pdf-merger](https://github.com/DALTCORE/lara-pdf-merger)
* **jenyaukraine** [AwesomeProject/pdf-merger](https://bitbucket.org/jenya_nonono/pdfmerge)
